import React from "react";
import ReactDOM from "react-dom";
import { shallow, configure } from 'enzyme';
import adapter from 'enzyme-adapter-react-16';
import App, { Todo } from "./App";

configure( { adapter: new adapter() } );

describe("App", () => {
  describe('Todo',()=> {

    it('ejecuta completeTodo cuando pincho en Complete', ()=> {

      //mokeo la función y sus parametros
      const completeTodo = jest.fn();
      const removeTodo = jest.fn();
      const index = 5;
      const todo = {
        isCompleted: true,
        text: 'lala'
      }
      // creao una constante de tipo shallow render para renderizar el componente
      const wrapper = shallow(
        <Todo 
          completeTodo={completeTodo}
          removeTodo={removeTodo}
          index={index}
          todo={todo}
        />
      );
      //busca un boton y simulo un click en el botton primero es decir el 0
      wrapper
        .find('button')
        .at(0)
        .simulate('click');

        // validar si es igual al valor index que le he pasado mokeado
        expect(completeTodo.mock.calls).toEqual([[5]]);
        // testeo que no hace click en el otro boton
        expect(removeTodo.mock.calls).toEqual([]);
    })
    it('ejecuta removeTodo cuando pincho en x', ()=> {

      //mokeo la función y sus parametros
      const completeTodo = jest.fn();
      const removeTodo = jest.fn();
      const index = 6;
      const todo = {
        isCompleted: true,
        text: 'lala'
      }
      // creao una constante de tipo shallow render para renderizar el componente
      const wrapper = shallow(
        <Todo 
          completeTodo={completeTodo}
          removeTodo={removeTodo}
          index={index}
          todo={todo}
        />
      );
      //busca un boton y simulo un click en el botton segundo es decir el 1
      wrapper
        .find('button')
        .at(1)
        .simulate('click');

        // validar si es igual al valor index que le he pasado mokeado
        expect(removeTodo.mock.calls).toEqual([[6]]);
        // testeo que no hace click en el otro boton
        expect(completeTodo.mock.calls).toEqual([]);
    })
  });
});
